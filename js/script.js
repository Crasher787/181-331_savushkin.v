

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-4481610-59', 'auto');
ga('send', 'pageview');





var buttonRead = document.getElementById('readButton');
var buttonWrite = document.getElementById('writeButton');
var inputX = document.getElementById('x');
var inputY = document.getElementById('y');
var inputRes = document.getElementById('result');

buttonRead.onclick = function(){
	var x = inputX.value;
	var y = inputY.value;
	var cells;
	var rows;

	rows = document.querySelectorAll('.table tr');
	cells = rows[x-1].getElementsByTagName('td');
	inputRes.value = cells[y-1].textContent;
}

buttonWrite.onclick = function(){
	var x = inputX.value;
	var y = inputY.value;
	var cells;
	var rows;
	var df = inputRes.value;

rows = document.querySelectorAll('.table tr');
cells = rows[x-1].getElementsByTagName('td');
cells[y-1].textContent = df;



}
